package com.cardoso.extrasimplemvpexample.data;

import com.cardoso.extrasimplemvpexample.interfaces.AlCuadrado;

/**
 * Created by Adrian on 29/12/2017.
 */

public class AlCuadradoModel implements AlCuadrado.Model {

    private AlCuadrado.Presenter presenter; /** Para comunicarse con el presentador*/
    private double result;

    public AlCuadradoModel(AlCuadrado.Presenter presenter){
        this.presenter = presenter;
    }

    @Override
    public void alCuadrado(String data) {
        try{
            result = Double.parseDouble(data)*Double.parseDouble(data);
            presenter.showResult(String.valueOf(result));
        }

        catch (Exception e){
            presenter.showError(e.getMessage());
        }


    }
}
