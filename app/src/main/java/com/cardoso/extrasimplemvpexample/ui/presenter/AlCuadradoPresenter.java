package com.cardoso.extrasimplemvpexample.ui.presenter;

import com.cardoso.extrasimplemvpexample.data.AlCuadradoModel;
import com.cardoso.extrasimplemvpexample.interfaces.AlCuadrado;

/**
 * Created by Adrian on 29/12/2017.
 */

public class AlCuadradoPresenter implements AlCuadrado.Presenter {
    private AlCuadrado.View view;     /** Para comunicarse con la vista*/
    private AlCuadrado.Model model;   /** Para comunicarse con el modelo */

    public AlCuadradoPresenter(AlCuadrado.View view){
        this.view = view;

        model = new AlCuadradoModel(this);
    }


    /** Comunicacion con la vista*/
    @Override
    public void showResult(String result) {
        if (view != null){
            view.showResult(result);
        }
    }

    @Override
    public void showError(String error) {
        if (view != null){
            view.showError(error);
        }


    }

    /**Comunicacion con el modelo */
    @Override
    public void alCuadrado(String data) {
        if (view != null){
            model.alCuadrado(data);
        }

    }
}
