package com.cardoso.extrasimplemvpexample.ui.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cardoso.extrasimplemvpexample.R;
import com.cardoso.extrasimplemvpexample.interfaces.AlCuadrado;
import com.cardoso.extrasimplemvpexample.ui.presenter.AlCuadradoPresenter;

public class AlCuadradoView extends AppCompatActivity implements AlCuadrado.View {
    /** WIDGETS */
    private EditText et_number;
    private TextView tv_result;

    /** Helper objects*/
    private AlCuadradoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defineWidgets();

        presenter = new AlCuadradoPresenter(this);
    }


    /** HANDLE ACTIONS*/
    public void calculate(View view){
        presenter.alCuadrado(et_number.getText().toString());
    }


    /** VIEW METHODS */
    @Override
    public void showResult(String result) {
        tv_result.setText(result);
    }


    @Override
    public void showError(String error) {
        Toast.makeText(this, "Error: " +  error, Toast.LENGTH_SHORT).show();
    }

    /**MY METHODS */
    private void defineWidgets(){
        et_number = findViewById(R.id.et_number);
        tv_result = findViewById(R.id.tv_result);
    }

}
