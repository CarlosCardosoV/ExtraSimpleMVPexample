package com.cardoso.extrasimplemvpexample.interfaces;

/**
 * Created by Adrian on 29/12/2017.
 */

/** */
public interface AlCuadrado {
    interface View{
        void showResult(String result);
        void showError(String error);
    }

    interface Presenter{
        void showResult(String result);
        void showError(String error);

        void alCuadrado(String data);
    }

    interface Model{
        void alCuadrado(String data);
    }
}
